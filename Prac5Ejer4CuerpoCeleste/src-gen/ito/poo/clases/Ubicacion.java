package ito.poo.clases;

import java.util.HashSet;

public class Ubicacion {
	
	private float longitud= 0F;;
	private float latitud= 0F;;
	private String periodo=  " ";
	private float distancia= 0F;;
	private HashSet<Ubicacion> localizacion;

	public Ubicacion() {
		super();
	}
	
	public Ubicacion(float longitud, float latitud, String periodo, float distancia) {
		super();
		this.longitud = longitud;
		this.latitud = latitud;
		this.periodo = periodo;
		this.distancia = distancia;
	}

	public float getDistancia() {
		return distancia;
	}

	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}

	public float getLongitud() {
		return this.longitud;
	}

	public float setLongitud() {
		return this.longitud;
	}

	public float getLatitud() {
		return this.latitud;
	}
	
	public float setLatitud() {
		return this.latitud;
	}

	public String getPeriodo() {
		return periodo;
	}

	public String toString() {
		return "Ubicacion [longitud=" + longitud + ", latitud=" + latitud + ", periodo=" + periodo + ", distancia=" + distancia + "]";
	}
}
